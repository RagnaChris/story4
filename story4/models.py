from django.db import models

# Create your models here.
class Jadwal(models.Model):
	name = models.CharField(max_length=30)
	date = models.DateField()
	time = models.TimeField()