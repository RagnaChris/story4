from django import forms
from django.forms import widgets


class JadwalForm(forms.Form):
	name = forms.CharField(label="Nama", widget=forms.TextInput())
	date = forms.DateField(label="Tanggal", widget=forms.SelectDateWidget(
		empty_label=("Choose Year", "Choose Month", "Choose Day"),),)
	time = forms.TimeField(label="Waktu")