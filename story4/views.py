from django.shortcuts import render
from .models import Jadwal
from .forms import JadwalForm

# Create your views here.

def new(request):
	return render(request, 'new.html')

def more(request):
	return render(request, 'more.html')

def get_jadwal(request):
	if request.method == 'POST':
		form = JadwalForm(request.POST)
		if form.is_valid():
			n = form.cleaned_data['name']
			d = form.cleaned_data['date']
			t = form.cleaned_data['time']
			jadwal = Jadwal(name=n, date=d, time=t)
			jadwal.save()
	else:
		form = JadwalForm()
	schedule = Jadwal.objects.all().order_by('date')
	return render(request, 'jadwal.html', {'form':form, "resp":schedule})

def delete_jadwal(request):
	obj_id = request.POST.get('id')
	obj = Jadwal.objects.get(id=obj_id)
	obj.delete()
	form = JadwalForm()
	schedule = Jadwal.objects.all().order_by('date')
	return render(request, 'jadwal.html', {'form':form, 'resp': schedule})