from django.urls import path
from . import views

urlpatterns = [
    path('', views.new, name='new'),
    path('more/', views.more, name='more'),
    path('jadwal/', views.get_jadwal, name='get_jadwal'),
    path('delete/', views.delete_jadwal, name='delete_jadwal'),
]